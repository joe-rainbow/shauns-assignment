import unittest
from processor.calculator import get_min
from data import load_data, Category

class test_calculator(unittest.TestCase):
    def setUp(self):
        self.filename = "C:\Code\Python\Sean\whr.csv"
        self.data = load_data(filename=self.filename)

    def tearDown(self):
        pass
    
    def test_get_min_for_social_support(self):
        value = get_min(self.data, Category.SOCIAL_SUPPORT)
        self.assertEqual(0.319589138, value, "Value mismatch")

if __name__ == '__main__':
    unittest.main()