
from enum import Enum
from processor import process_cell

class Category(Enum):
    COUNTRY = 1
    LIFE_LADDER = 2
    LOG_GDP_PER_CAPITA = 3
    SOCIAL_SUPPORT = 4
    HEALTHY_LIFE_EXPECTANCY_AT_BIRTH = 5
    FREEDOM_TO_MAKE_LIFE_CHOICES = 6
    GENEROSITY = 7
    CONFIDENCE_IN_NATIONAL_GOVERNMENT = 8

class WHRData_row():
    """
    Class to represent one row in the csv
    """
    def __init__(self):
        self.country = ""
        self.life_ladder = ""
        self.log_GDP = ""
        self.social_support = ""
        self.life_expentancy = ""
        self.freedom_choices = ""
        self.generosity = ""
        self.confidence_in_govt = ""

    def set_country(self, country):
        self.country = country

    def set_life_ladder(self, life_ladder):
        self.life_ladder = life_ladder

    def set_log_GDP(self, log_GDP):
        self.log_GDP = log_GDP
    
    def set_social_support(self, social_support):
        self.social_support = social_support

    def set_life_expentancy(self, life_expentancy):
        self.life_expentancy = life_expentancy

    def set_freedom_choices(self, freedom_choices):
        self.freedom_choices = freedom_choices

    def set_generosity(self, generosity):
        self.generosity = generosity

    def set_confidence_in_govt(self, confidence_in_govt):
        self.confidence_in_govt = confidence_in_govt

class WHRData():
    """
    Class to represent an entire csv file
    """
    def __init__(self):
        self.rows = []

    def add_row(self, row):
        self.rows.append(row)


def load_data(filename):
    data = WHRData()
    open_file = open(filename, 'r')
    csvfile_data = open_file.readlines()
    row_counter = 0
    for i in csvfile_data:
        if row_counter == 0:
            #Skip headers
            row_counter = row_counter + 1
        else:
            row = WHRData_row()
            x = i.strip('\n')
            #print(x.spl7#it(','))
            column_counter = 1
            for cell in x.split(','):
                process_cell(cell, row, column_counter)
                column_counter = column_counter + 1
            data.add_row(row)
            print("Country {0}".format(row.country))
            print("Social Support {0}".format(row.social_support))
    return data
