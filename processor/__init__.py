

def process_cell(cell,row,column_counter):
    print("Cell {0}".format(cell))
    print("Column Counter {0}".format(column_counter))
    #input("Press to continue")
    if column_counter == 1:
        if cell:
            if cell.isalpha():
                print("setting country name to {0}".format(cell))
                row.set_country(cell)
            else:
                row.set_country("Invalid country")
        else:
            row.set_country("No country name")
    if column_counter == 2:
        if cell:
            row.set_life_ladder(cell)
        else:
            row.set_life_ladder(0)
    if column_counter == 3:
        if cell:
            row.set_log_GDP(cell)
        else:
            row.set_log_GDP(0)
    if column_counter == 4:
        if cell:
            row.set_social_support(cell)
        else: 
            row.set_social_support("Empty")
    if column_counter == 5:
        if cell:
            row.set_life_expentancy(cell)
        else:
            row.set_life_expentancy(0)
    if column_counter == 6:
        if cell:
            row.set_freedom_choices(cell)
        else:
            row.set_freedom_choices(0)
    if column_counter == 7:
        if cell:
            row.set_generosity(cell)
        else:
            row.set_generosity(0)
    if column_counter == 8:
        if cell:
            row.set_confidence_in_govt(cell)
        else:
            row.set_confidence_in_govt(0)
