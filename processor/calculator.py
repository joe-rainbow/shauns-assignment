from data import Category

def get_min(data, category):
    if category == Category.SOCIAL_SUPPORT:
        #This is a list comprehension read https://www.pythonforbeginners.com/basics/list-comprehensions-in-python 
        #It is design to be a short and sweet solution to a complex iterative problem.
        #I am using a conditional list comprehension to sort out issues with missing content in the cell.
        social_support_values = [float(row.social_support) for row in data.rows if row.social_support.isalpha() is False]
        social_support_values.sort()
        return social_support_values[0]
