import os, sys
from data import WHRData_row, WHRData, Category, load_data
from processor import process_cell
from processor.calculator import get_min

f = open('results.txt','w')
sys.stdout = f


filename = "C:\Code\Python\Sean\whr.csv"

data = load_data(filename)

min_value = get_min(data,Category.SOCIAL_SUPPORT) 

print("Min Value = {0}".format(min_value))
f.close()