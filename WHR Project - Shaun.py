# 'to open a file from users direct path'
def happiness():
    file_location = input("please enter the location of the file: ")
    open_file = open(file_location, 'r')
    csvfile_data = open_file.readlines()
    for i in csvfile_data:
        x = i.strip('\n')
        print(x.split(','))

    open_file.close()

happiness()

# 'to find the average of the data set' (working)
def average_column():
    with open("/users/shaunsamuel/pycharmProjects/w.csv", "r") as filename:
        average = 0
        Sum = 0
        row_count = 0
        for row in filename:
            for column in row.split(','):
                n = float(column)
                Sum += n
                row_count += 1

        average = Sum / len(column)
        filename.close()

        return 'The average is:', average

# 'second attempt to find the average of the data set (used a small reduced matrix)

def average_column():
    f = open('/Users/shaunsamuel/PycharmProjects/w.csv', "r")

    for r in range(1, 7):
        for c in range(2, 8):
            sum_cols = [sum(x) for x in zip(*f)]
    print(sum_cols)


average_column()

# 'attempt to find the other metrics of the input data set'
number_file_name = 'file name'
number_sum = 0
number_count = 0
number_average = 0
number_maximum = 0
number_minimum = 0
number_range = 0

do_calculation = True
while(do_calculation):

    while (True):
        try:
            # Get the name of a file
            number_file_name = input('Enter a filename. Be sure to include .txt after the file name: ')
            random_number_count = 0

            print('')

            random_number_file = open(number_file_name, "r")
            print ('File Name: ', number_file_name, ':', sep='')

            print('')

            numbers = random_number_file.readlines()
            random_number_file.close
        except:
            print('An error occured trying to read', random_number_file)
        else:
            break

    try:
        number_file = open(number_file_name, "r")

        is_first_number = True

        for number in number_file:
            number = int(number)  # convert the read string to an int

            if (is_first_number):
                number_maximum = number
                number_minimum = number
                is_first_number = False

            number_sum += number
            number_count += 1
            if (number > number_maximum):
                number_maximum = number
            if (number < number_minimum):
                number_minimum = number

        number_average = number_sum / number_count
        number_range = number_maximum - number_minimum

        index = 0
        listnumbers = 0
        while index < len(numbers):
            numbers[index] = int(numbers[index])
            index += 1


        number_file.close()
    except Exception as err:
        print ('An error occurred reading', number_file_name)
        print ('The error is', err)
    else:
        print ('Sum: ', number_sum)
        print ('Count:', number_count)
        print ('Average:', number_average)
        print ('Maximum:', number_maximum)
        print ('Minimum:', number_minimum)
        print ('Range:', number_range)
        print ('Median:', median)

    another_calculation = input("Do you want to enter in another file name? (y/n): ")
    if(another_calculation !="y"):
        do_calculation = False


# 'this code is written using modules which arnt permitted for the assignment but seem to be working'

#import numpy as np


#df2["Mean"]=df2.mean(axis=0)  # adds a new row for median

import pandas as pd
import scipy as stats
from scipy import stats

df = pd.read_csv (r'/Users/shaunsamuel/PycharmProjects/whr.csv')
#print (df)

# To find the mean of Log GDP per capita
gdp_min = df['Log GDP per capita'].min()
gdp_mean = df['Log GDP per capita'].mean()
gdp_median = df['Log GDP per capita'].median()

print(gdp_min, gdp_mean, gdp_median)
# To find the mean of Log GDP per capita

ss_min = df['Social support'].min()
ss_mean = df['Social support'].mean()
ss_median = df['Social support'].median()

# To find the mean of Log GDP per capita

hleb_min = df['Healthy life expectancy at birth'].min()
hleb_mean = df['Healthy life expectancy at birth'].mean()
hleb_median = df['Healthy life expectancy at birth'].median()

# To find the mean of Log GDP per capita

gen_min = df['Generosity'].min()
gen_mean = df['Generosity'].mean()
gen_median = df['Generosity'].median()

# To find the mean of Log GDP per capita

conf_min = df['Confidence in national government'].min()
conf_mean = df['Confidence in national government'].mean()
conf_median = df['Confidence in national government'].median()



df.mean(axis=0)
print(df.mean(axis=0))

df.median(axis=0)
print(df.median(axis=0))

df.min(axis=0)
print(df.min(axis=0))