import os

class WHRData_row():
    """
    Class to represent one row in the csv
    """
    def __init__(self):
        self.country = ""
        self.life_ladder = ""
        self.log_GDP = ""
        self.social_support = ""
        self.life_expentancy = ""
        self.freedom_choices = ""
        self.generosity = ""
        self.confidence_in_govt = ""

    def set_country(self, country):
        self.country = country

    def set_life_ladder(self, life_ladder):
        self.life_ladder = life_ladder

    def set_log_GDP(self, log_GDP):
        self.log_GDP = log_GDP
    
    def set_social_support(self, social_support):
        self.social_support = social_support

    def set_life_expentancy(self, life_expentancy):
        self.life_expentancy = life_expentancy

    def set_freedom_choices(self, freedom_choices):
        self.freedom_choices = freedom_choices

    def set_generosity(self, generosity):
        self.generosity = generosity

    def set_confidence_in_govt(self, confidence_in_govt):
        self.confidence_in_govt = confidence_in_govt

class WHRData():
    """
    Class to represent an entire csv file
    """
    def __init__(self):
        self.rows = []

    def add_row(self, row):
        self.rows.append(row)

def load_data(filename):
    open_file = open(filename, 'r')
    csvfile_data = open_file.readlines()
    counter = 0
    for i in csvfile_data:
        if counter == 0:
            pass
        else:
            row = WHRData_row()
            x = i.strip('\n')
            #print(x.spl7#it(','))
            column_counter = 1
            for cell in x.split(','):
                print(cell)
                #input("Press to continue")
                if column_counter == 1:
                    row.set_country(cell)
                if column_counter == 2:
                    row.set_life_ladder(cell)
                if column_counter == 3:
                    if cell:
                        row.set_log_GDP(cell)
                    else:
                        row.set_log_GDP("Empty")
                
        counter = counter + 1

filename = "C:\Code\Python\Sean\whr.csv"

load_data(filename)